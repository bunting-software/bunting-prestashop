# Installing Bunting on Prestashop

To download the plugin click on Downloads in the left hand menu then click the `buntingpersonalisation.zip` link.

Next to install the `bunting-prestashop` plugin, go the admin dashboard of your site and within the menu on the left click `Modules`,
this will navigate you to a module listing page on the top right of this page should be an `Upload a module` button.

Here please upload the `buntingpersonalisation.zip` if everything works correctly it should take you to `Module installed!`.

>Please note that this file must be a `.zip` file. If you downloaded the `bunting-prestashop` folder just zip up the 
folder and then retry the upload.
 
Either following the configure link upon installing the module or via the installed plugins list under 
`Modules -> Installed Modules`.

Sign in to your Bunting account to complete the installation process: Enter your existing bunting account details or
if you are new to Bunting and want to get started, click the `CREATE ACCOUNT` button.