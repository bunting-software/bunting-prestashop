<?php
/**
 * 2016 Bunting Software Ltd
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Bunting Software Ltd <hello@bunti.ng>
 * @copyright 2016 Bunting Software Ltd
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class BuntingPersonalisation extends Module
{
    public function __construct()
    {
        $this->name = 'buntingpersonalisation';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0.3';
        $this->author = 'Bunting';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->module_key = '6df9ad7c2ab37a34467ed38bcfa47956';

        parent::__construct();

        $this->displayName = $this->l('Bunting Personalization');
        $this->description = $this->l('The easy way to boost sales with personalization and product recommendations.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->installTab();
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->unlinkBunting() ||
            !$this->uninstallTab()
        ) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        if (Tools::getValue('subdomain')) {
            echo $this->subdomainExists(Tools::getValue('subdomain'));
            exit();
        }

        if (Tools::getValue('verify_email_address') || Tools::getValue('register_email_address')) {
            if (Tools::getValue('verify_email_address')) {
                return $this->buntingVerify();
            } elseif (Tools::getValue('register_email_address')) {
                return $this->buntingRegister();
            }
        }

        $this->context->controller->addCSS($this->_path.'views/css/admin.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/bootstrap.min.css', 'all');
        $this->context->controller->addJS($this->_path.'views/js/admin.js', 'all');
        $this->context->controller->addJS($this->_path.'views/js/validate.js', 'all');
        $this->context->smarty->assign(
            array(
                'bunting_header' => $this->local_path.'views/templates/admin/header.tpl',
                'bunting_footer' => $this->local_path.'views/templates/admin/footer.tpl',
            )
        );
        if (Tools::getValue('unlink')) {
            $this->unlinkBunting();
            $uri = $this->removeqsvar('unlink', $_SERVER['REQUEST_URI']);
            $protocol = Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://';
            $this->context->smarty->assign(array('install_url' => $protocol.$_SERVER['HTTP_HOST'].$uri));
            return $this->context->smarty->fetch($this->local_path.'views/templates/admin/unlinked.tpl');
        }
        if (Configuration::get('BUNTING_ACCOUNT_ID')) {
            return $this->displayHome();
        }
        return $this->displayInstallForm();
    }

    private function removeqsvar($varname, $url)
    {
        list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
        parse_str($qspart, $qsvars);
        unset($qsvars[$varname]);
        $newqs = http_build_query($qsvars);
        return $urlpart . '?' . $newqs;
    }

    /**
     * Checks if the subdomain passed as a parameter exists within bunting
     *
     * @param string $subdomain
     * @return int
     */
    private function subdomainExists($subdomain){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://' . $subdomain . '.1.bunting.com/login?a=lost_password');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_exec($ch);

        return (int) (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200);
    }

    private function displayInstallForm()
    {
        $shopName = Configuration::get('PS_SHOP_NAME');
        $locale = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        $this->context->smarty->assign(
            array(
                'shop_owner_email' => Configuration::get('PS_SHOP_EMAIL'),
                'shop_name' => $shopName,
                'potential_subdomain' => Tools::strtolower(preg_replace("/[^A-Za-z0-9]/", '', $shopName)),
                'phone' => Configuration::get('BLOCKCONTACTINFOS_PHONE')
            )
        );
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/install.tpl');
    }

    private function displayHome()
    {
        $timestamp = time();
        $bunting_subdomain = Configuration::get('BUNTING_SUBDOMAIN');
        $bunting_region_id = Configuration::get('BUNTING_REGION_SUBDOMAIN_ID');
        $bunting_email = Configuration::get('BUNTING_EMAIL');
        $password_api = Configuration::get('BUNTING_PASSWORD_API');
        $account_key = $bunting_subdomain.$bunting_email.$timestamp;

        $message = '';
        if (isset($this->context->cookie->bunting_message)) {
            $message = $this->context->cookie->bunting_message;
            $this->context->cookie->__unset('bunting_message');
        }

        $this->context->smarty->assign(
            array(
                'bunting_subdomain' => $bunting_subdomain,
                'bunting_region_id' => $bunting_region_id,
                'timestamp' => $timestamp,
                'hash' => hash_hmac('sha256', $account_key, 'fKJ83-d*lc-29)s'),
                'password_api' => $password_api,
                'email_address' => $bunting_email,
                'message' => $message
            )
        );

        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/home.tpl');
    }

    private function buntingVerify()
    {
        $bunting_data = $this->submitToBunting('verify', [
            'email_address' => Tools::getValue('verify_email_address'),
            'password' => Tools::getValue('verify_password'),
            'subdomain' => Tools::getValue('verify_bunting_subdomain')
        ]);
        return $this->buntingResponse($bunting_data);
    }

    private function buntingRegister()
    {
        $locale = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        $address = Configuration::get('BLOCKCONTACTINFOS_ADDRESS');
        $address = $this->processAddress($address);

        $submit_data = [
            'billing' => 'automatic',
            'email_address' => Tools::getValue('register_email_address'),
            'password' => Tools::getValue('register_password'),
            'confirm_password' => Tools::getValue('password_confirmation'),
            'subdomain' => Tools::getValue('register_bunting_subdomain'),
            'name' => Tools::getValue('company_name'),
            'forename' => Tools::getValue('forename'),
            'surname' => Tools::getValue('surname'),
            'telephone_number' => Tools::getValue('telephone_number'),
            'promotional_code' => Tools::getValue('promotional_code'),
            'timezone' => Configuration::get('PS_TIMEZONE'),
            'country' => $locale,
            'agency' => 'no'
        ];

        $submit_data = $submit_data+$address;

        $bunting_data = $this->submitToBunting('register', $submit_data);
        return $this->buntingResponse($bunting_data);
    }

    private function processAddress($orig_address)
    {
        $comma_address = preg_replace('#\s+#', ',', trim($orig_address));
        $address_array = explode(',', $comma_address);
        $address = [];
        if (!empty($address_array)) {
            $address['address_line_1'] = array_shift($address_array);
            $address['postcode'] = array_pop($address_array);
            $i = 2;
            foreach ($address_array as $address_line) {
                $address['address_line_'.$i] = $address_line;
                if ($i < 5) {
                    $i++;
                } else {
                    break;
                }
            }
        } else {
            $address['address_line_1'] = $comma_address;
        }
        ksort($address);
        return $address;
    }

    private function submitToBunting($action, $params)
    {

        $languages = [];
        $currencies = [];

        foreach (Currency::getCurrencies() as $currency) {
            $currencies[] = [
                'currency' => $currency['iso_code'],
                'symbol' => $currency['sign']
            ];
        }

        foreach (Language::getLanguages() as $language) {
            $languages[] = Tools::strtoupper($language['iso_code']);
        }

        $domain = Configuration::get('PS_SHOP_DOMAIN');
        $locale = Configuration::get('PS_LOCALE_LANGUAGE');

        $timestamp = time();

        $feed_token = md5(Configuration::get('PS_NEWSLETTER_RAND').$timestamp);
        $link = new \Link;

        $default_params = [
            'timestamp' => $timestamp,
            'hash' => hash_hmac('sha256', $timestamp, 'fKJ83-d*lc-29)s'),
            'plugin' => 'prestashop',
            'domain_name' => $domain,
            'create_website_monitor' => 'yes',
            'website_name' => Configuration::get('PS_SHOP_NAME'),
            'languages' => $languages,
            'currencies' => $currencies,
            'website_platform' => 'PrestaShop',
            'ecommerce' => 'yes',
            'cart_url' => $this->context->link->getPageLink('cart'),
            'product_feed-url_protocol' => Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://',
            'product_feed-url' => $link->getModuleLink('buntingpersonalisation', 'xml', ['feed_token' => $feed_token]),
        ];
        $params = $params+$default_params;
        $params = $this->httpBuildQueryForCurl($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bunting.com/plugins/'.$action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $data = Tools::jsonDecode($response, true);
        if ($data['success']) {
            $data['email_address'] = $params['email_address'];
            $this->installBunting($data, $feed_token);
        }
        return $data;
    }

    private function buntingResponse($buntingData)
    {
        $response = [];

        if ($buntingData['success']) {
            $response['message'] = $_SESSION['message'] = 'You can now login to Bunting.';
            echo Tools::jsonEncode($response);
            die;
        }

        $response['message'] = 'Please review the errors and try again.';

        if (!isset($buntingData['errors']) || !count($buntingData['errors'])) {
            $buntingData['errors'] = [];
            $response['message'] .= '<br><br>There was a problem connecting your shop to Bunting, please contact Bunting support.';
        }

        if (isset($buntingData['errors']['validation'])) {
            $response['message'] .= '<br><br>' . $buntingData['errors']['validation'];
        }

        $response['errors'] = $buntingData['errors'];
        echo Tools::jsonEncode($response);
        die();
    }

    private function httpBuildQueryForCurl($arrays, &$new = array(), $prefix = null)
    {
        if (is_object($arrays)) {
            $arrays = get_object_vars($arrays);
        }

        foreach ($arrays as $key => $value) {
            $k = isset($prefix) ? $prefix.'['.$key.']':$key;
            if (is_array($value) || is_object($value)) {
                $this->httpBuildQueryForCurl($value, $new, $k);
            } else {
                $new[$k] = $value;
            }
        }
        return $new;
    }

    private function installBunting($data, $feed_token)
    {
        Configuration::updateValue('BUNTING_ACCOUNT_ID', $data['account_id']);
        Configuration::updateValue('BUNTING_EMAIL', $data['email_address']);
        Configuration::updateValue('BUNTING_WEBSITE_MONITOR_ID', $data['website_monitor_id']);
        Configuration::updateValue('BUNTING_UNIQUE_CODE', $data['unique_code']);
        Configuration::updateValue('BUNTING_SUBDOMAIN', $data['subdomain']);
        Configuration::updateValue('BUNTING_REGION_SUBDOMAIN_ID', $data['server_region_subdomain_id']);
        Configuration::updateValue('BUNTING_FEED_TOKEN', $feed_token);
        Configuration::updateValue('BUNTING_PASSWORD_API', $data['password_api']);
    }

    private function unlinkBunting()
    {
        Configuration::deleteByName('BUNTING_ACCOUNT_ID');
        Configuration::deleteByName('BUNTING_EMAIL');
        Configuration::deleteByName('BUNTING_WEBSITE_MONITOR_ID');
        Configuration::deleteByName('BUNTING_UNIQUE_CODE');
        Configuration::deleteByName('BUNTING_SUBDOMAIN');
        Configuration::deleteByName('BUNTING_REGION_SUBDOMAIN_ID');
        Configuration::deleteByName('BUNTING_FEED_TOKEN');
        Configuration::deleteByName('BUNTING_PASSWORD_API');
        return true;
    }

    public function hookdisplayHeader($params)
    {
        $smarty_vars = [];
        $tempVars = $this->context->smarty->tpl_vars;

        $pageName = isset($tempVars['page']) && isset($tempVars['page']->value['page_name']) ? $tempVars['page']->value['page_name'] : '';
        //$pageName = isset($tempVars['page_name']) && $pageName === '' ? $tempVars['page_name']->value : '';

        if ($pageName == 'order-confirmation' && Tools::getValue('id_order')) {
            $order = new Order(Tools::getValue('id_order'));

            if ($order->secure_key == Tools::getValue('key') && $order->id_cart) {
                $smarty_vars['bunting_order'] = $order;
                $this->context->cookie->__unset('track_checkout');
            }
        }

        if ($this->context->customer->isLogged()) {
            $smarty_vars['bunting_customer'] = [
                'id' => $this->context->customer->id,
                'firstname' => $this->context->customer->firstname,
                'lastname' => $this->context->customer->lastname,
                'email' => $this->context->customer->email
            ];
        }

        if (isset($_GET['id_category'])) {
            $category = new Category($_GET['id_category'], intval(Configuration::get('PS_LANG_DEFAULT')));
            if (Validate::isLoadedObject($category)) {
                $parents = $category->getParentsCategories();
                $this_cat_names = [];
                foreach($parents as $parent) {
                    $this_cat_names[] = $parent['name'];
                }
                $this_cat_names = array_reverse($this_cat_names);
                $smarty_vars['bunting_category'] = implode('>', $this_cat_names);
            }
        }

        if (version_compare(_PS_VERSION_, "1.7.0.0", "<")) {
            $smarty_vars['page'] = array(
                'page_name' => $pageName
            );

            if ($pageName === "product") {
                $smarty_vars['productId'] = Tools::getValue('id_product');
            }
        }

        if ($pageName === "product") {
            $smarty_vars['productLanguage'] = strtoupper(substr($this->context->language->language_code, 0, 2));
            $smarty_vars['productName'] = isset($tempVars['name']) ? $tempVars['name']->value : '';
            $smarty_vars['productCurrency'] = isset($tempVars['currency']) && isset($tempVars['currency']->value->iso_code) ? $tempVars['currency']->value->iso_code : '';
            $smarty_vars['productPrice'] = Product::getPriceStatic(Tools::getValue('id_product'), true, null, 6);
            $smarty_vars['productOriginalPrice'] = Product::getPriceStatic(Tools::getValue('id_product'), true, false, 6, null, false, false);
            $smarty_vars['onSale'] = $smarty_vars['productPrice'] != $smarty_vars['productOriginalPrice'] ? 'y' : 'n';
            $smarty_vars['productSaving'] = number_format($smarty_vars['productOriginalPrice'] - $smarty_vars['productPrice'], 2);
            $smarty_vars['productPrice'] = number_format($smarty_vars['productPrice'], 2);

            $imageId = Product::getCover(Tools::getValue('id_product'));

            if (is_array($imageId)) {
                $smarty_vars['productImage'] = $this->context->link->getImageLink(Tools::getValue('id_product'), $imageId['id_image'], 'large_default');
            }

            $product = new Product(Tools::getValue('id_product'));
            $smarty_vars['productAvailability'] = property_exists($product, 'available_for_order') && (int) $product->available_for_order ? "y" : "n";
            $smarty_vars['productRrp'] = property_exists($product, 'rrp') && is_scalar($product->rrp) ? number_format($product->rrp, 2) : 0;
        }

        if ($pageName === "cart") {
            $smarty_vars['bunting_cart_products'] = Context::getContext()->cart->getProducts();
        }

        if (version_compare(_PS_VERSION_, "1.7.2.0", ">")) {
            $smarty_vars['page_name'] = $pageName;

            $this->context->smarty->assign(
                $smarty_vars,
                $smarty_vars
            );
        }


        if (Configuration::get('BUNTING_ACCOUNT_ID') && get_class(Context::getContext()->controller) !== "BuntingPersonalisationXmlModuleFrontController") {
            $this->context->smarty->assign('bunting', [
                'subdomain' => Configuration::get('BUNTING_SUBDOMAIN'),
                'website_monitor_id' => Configuration::get('BUNTING_WEBSITE_MONITOR_ID'),
                'unique_code' => Configuration::get('BUNTING_UNIQUE_CODE')
            ]);
            return $this->context->smarty->fetch($this->local_path . 'views/templates/hooks/head.tpl');
        }

        return '';
    }

    public function installTab()
    {
        if (_PS_VERSION_ < '1.5') {
            return true;
        }

        $languages = Language::getLanguages(true);
        if (empty($languages)) {
            return false;
        }

        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'BuntingPersonalisation';
        $tab->name = array();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = 'Bunting Personalization';
        }
        $tab->id_parent = 0;
        $tab->module = 'buntingpersonalisation';
        $added = $tab->add();

        if ($added && _PS_VERSION_ < '1.6') {
            $tab = new Tab();
            $tab->active = 1;
            $tab->class_name = 'BuntingPersonalisationMenu';
            $tab->name = array();
            foreach ($languages as $lang) {
                $tab->name[$lang['id_lang']] = 'Manage';
            }
            $tab->id_parent = (int)Tab::getIdFromClassName('BuntingPersonalisation');
            $tab->module = 'buntingpersonalisation';
            $added = $tab->add();
        }

        return $added;
    }

    public function uninstallTab()
    {
        if (_PS_VERSION_ < '1.5') {
            return true;
        }

        foreach (array('BuntingPersonalisation', 'BuntingPersonalisationMenu') as $tab_name) {
            $id_tab = (int)Tab::getIdFromClassName($tab_name);
            if ($id_tab) {
                $tab = new Tab($id_tab);
                $tab->delete();
            }
        }

        return true;
    }

    public function hookDisplayBackOfficeHeader()
    {
        $ctrl = $this->context->controller;
        if ($ctrl instanceof AdminController && method_exists($ctrl, 'addCss')) {
            $ctrl->addCss($this->_path.'views/css/admin-menu.css');
        }
    }
}
