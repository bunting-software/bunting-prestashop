{*
* 2016 Bunting Software Ltd
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    Bunting Software Ltd <hello@bunti.ng>
* @copyright 2016 Bunting Software Ltd
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{include file="$bunting_header"}
<div class="box-container logged-in">
    {if $message != ''}
        <h2 class="title">Bunting added <strong>Successfully</strong></h2>
        <p>{$message|escape:'htmlall':'UTF-8'}</p>
    {else}
        <h2 class="title">Sign in to <strong>Bunting</strong></h2>
    {/if}
    <p>
        Account address:<br><strong>{$bunting_subdomain|escape:'htmlall':'UTF-8'}.{$bunting_region_id|escape:'htmlall':'UTF-8'}bunting.com</strong>
    </p>
    <form action="https://{$bunting_subdomain|escape:'htmlall':'UTF-8'}.bunting.com/login" class="dotted go-to-bunting" target="_blank" method="post">
        <input type="hidden" name="a" value="login">
        <input type="hidden" name="timestamp" value="{$timestamp|escape:'htmlall':'UTF-8'}">
        <input type="hidden" name="hash" value="{$hash|escape:'htmlall':'UTF-8'}">
        <input type="hidden" name="password_api" value="{$password_api|escape:'htmlall':'UTF-8'}">
        <input type="hidden" name="email_address" value="{$email_address|escape:'htmlall':'UTF-8'}">
        <input type="hidden" name="plugin" value="prestashop">
        {if $message != ''}
            <input type="hidden" name="redirect" value="/account?a=show_registration_complete">
        {/if}
        <button type="submit" class="btn btn-info">
            Login to your<br>Bunting Account
        </button>
    </form>
    <a href="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}&unlink=true" onclick="return confirm('Are you sure you want to unlink your Bunting account?');" class="unlink-bunting btn btn-default">
        Uninstall
    </a>
</div>
<script>
    setInterval(function() {
        window.location.reload();
    }, 58000);
</script>
{include file="$bunting_footer"}