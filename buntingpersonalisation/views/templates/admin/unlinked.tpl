{*
* 2016 Bunting Software Ltd
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    Bunting Software Ltd <hello@bunti.ng>
* @copyright 2016 Bunting Software Ltd
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{include file="$bunting_header"}
<div class="box-container">
    <h2 class="title">Bunting scripts <strong>removed successfully</strong></h2>
    <p>The Bunting tracking script has been successfully removed from your store. Bunting is no longer tracking your visitors.</p>
    <br><br>
    <h3>Uninstalling the app</h3>
    <p>Go to "Modules and Services" in the admin menu to the left, search for Bunting Personalization, click uninstall in the dropdown menu.</p>
    <br>
    <p><strong>We're sorry to see you go, but thank you for using Bunting!</strong></p>
    <br>
    <a href="{$install_url|escape:'htmlall':'UTF-8'}" class="btn btn-primary btn-lg" >Back to the install page</a>
</div>
{include file="$bunting_footer"}