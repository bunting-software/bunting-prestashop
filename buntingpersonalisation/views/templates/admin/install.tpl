{*
* 2016 Bunting Software Ltd
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    Bunting Software Ltd <hello@bunti.ng>
* @copyright 2016 Bunting Software Ltd
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{include file="$bunting_header"}
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet" />
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<div class="box-container" id="loginForm">
    <div class="title-logo"><img src="https://bunting.com/media/logo.svg"></div>
    <p style="display: none" class="message"></p>
    <div class="row">
        <div class="underlined">
            <h4>New to Bunting?</h4>
            <a href="#planChooseForm" class="btn btn-primary btn-lg" id="choosePlanButton">Create Account</a>
        </div>
        <div class="login underlined">
            <h2>Already have a Bunting Account?</h2>
            <form id="actualLoginForm">
                <div class="form-group required">
                    <label for="verify_bunting_subdomain">Bunting account subdomain</label>
                    <input type="text" class="form-control" id="verify_bunting_subdomain" name="verify_bunting_subdomain" required>
                </div>
                <div class="form-group required">
                    <label for="verify_email_address">Email address</label>
                    <input type="email" class="form-control" id="verify_email_address" name="verify_email_address" value="{$shop_owner_email|escape:'htmlall':'UTF-8'}" required>
                </div>
                <div class="form-group required">
                    <label for="verify_password">Password</label>
                    <input type="password" class="form-control" id="verify_password" name="verify_password" required>
                </div>
                <button type="submit" class="btn btn-info">Login</button>
            </form>
        </div>
        <div>
            Help: <a class="forgotPasswordTrigger" href="https://bunting.com/contact" target="_blank">I've lost my password</a>
            <form class="forgotPasswordForm">
                <div class="form-group required">
                    <label for="verify_bunting_subdomain">Bunting account subdomain</label>
                    <input type="text" class="form-control" id="bunting_forgot_subdomain" name="bunting_forgot_subdomain" required="">
                </div>
                <button type="submit" class="btn btn-primary btn-large btn-last">Reset</button>
            </form>
        </div>
    </div>
</div>
<div class="box-container wide" id="registerForm" style="display:none;">
    <a class="back" href="#back">Back</a>
    <h2 class="title">Create Your <strong>Free Trial Account</strong></h2>
    <h4>You're moments away from personalising your website</h4>
    <form id="actualRegisterForm">
        <fieldset>
            <h3>Your Bunting Account</h3>
            <div class="form-group required">
                <label for="company_name">Company Name</label>
                <input type="text" class="form-control" id="company_name" name="company_name" value="{$shop_name|escape:'htmlall':'UTF-8'}" required>
            </div>
            <div class="form-group required">
                <label for="register_bunting_subdomain">Choose your Bunting account's web address</label>
                <div class="input-group">
                    <div class="input-group-addon">https://</div>
                    <input type="text" class="form-control" id="register_bunting_subdomain" name="register_bunting_subdomain" value="{$potential_subdomain|escape:'htmlall':'UTF-8'}" required>
                    <div class="input-group-addon">.bunting.com</div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <h3>Your Login Details</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group required">
                        <label for="forename">Forename</label>
                        <input type="text" class="form-control" id="forename" name="forename" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group required">
                        <label for="surname">Surname</label>
                        <input type="text" class="form-control" id="surname" name="surname" required>
                    </div>
                </div>
            </div>
            <div class="form-group required">
                <label for="register_email_address">Email Address</label>
                <input type="email" class="form-control" id="register_email_address" name="register_email_address" value="{$shop_owner_email|escape:'htmlall':'UTF-8'}" required>
                <span class="hint">(Kept safe, never given to others)</span>
            </div>
            <div class="form-group required">
                <label for="telephone_number">Phone</label>
                <input type="text" class="form-control" id="telephone_number" name="telephone_number" value="{$phone|escape:'htmlall':'UTF-8'}" required>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group required">
                        <label for="register_password">Password</label>
                        <input type="password" class="form-control" id="register_password" name="register_password" required>
                        <span class="hint">(We'll encrypt this for security)</span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group required">
                        <label for="password_confirmation">Confirm password</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <p>
                    Do you have a <a id="promoCodeButton" role="button" data-toggle="collapse" href="#promo-code" aria-expanded="false" aria-controls="promo-code">Promo Code</a>?
                </p>
                <div class="collapse" id="promo-code">
                    <input type="text" class="form-control" id="promotional_code" name="promotional_code" placeholder="Promo code (optional)">
                </div>
            </div>
            <div class="submit">
                <div class="row">
                    <div class="col-sm-6 submit-text">
                        <span id="premium-terms">By clicking the button you agree to Bunting's <a href="http://knowledgebase.getbunting.com/bunting-terms-of-service/" target="_blank">Terms of Business</a>.</span>
                    </div>
                    <div class="col-sm-6 submit-button">
                        <button type="submit" class="btn btn-info">Get started</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>
{include file="$bunting_footer"}