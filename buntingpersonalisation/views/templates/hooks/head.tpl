{*
* 2016 Bunting Software Ltd
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    Bunting Software Ltd <hello@bunti.ng>
* @copyright 2016 Bunting Software Ltd
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{if $bunting}
    {if $page['page_name'] == 'product'}
    {literal}
        <script type="text/javascript">
            if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}};
            $_Bunting.d.vp_upc = "{/literal}{$product.id_product}{$productId}{literal}";
            $_Bunting.d.vp_ns_{/literal}{$productLanguage}{literal} = "{/literal}{$productName}{literal}";
            $_Bunting.d.vp_ps_{/literal}{$productCurrency}{literal} = "{/literal}{$productPrice}{literal}";
            {/literal}{if $onSale == "y"}{literal}
            $_Bunting.d.vp_oss_{/literal}{$productCurrency}{literal} = "{/literal}{$productSaving}{literal}";
            {/literal}{/if}{literal}
            $_Bunting.d.vp_iu = "{/literal}{$productImage}{literal}";
            $_Bunting.d.vp_s = "{/literal}{$productAvailability}{literal}";
            $_Bunting.d.vp_c = "Root";
            {/literal}{if $productRrp != 0}{literal}
            $_Bunting.d.vp_cv1 = "{/literal}{$productRrp}{literal}";
            {/literal}{/if}{literal}
        </script>
    {/literal}
    {elseif $page['page_name'] == 'cart'}
    {literal}
        <script type="text/javascript">
            if (typeof $_Bunting=="undefined") var $_Bunting={d:{}};
            $_Bunting.d.cp = new Array();

            var bunting_shipping = {/literal}{$bunting_shipping|escape:'htmlall':'UTF-8'}{literal};
            bunting_shipping = bunting_shipping.toFixed(2);
            $_Bunting.d.cdc = bunting_shipping.toString();
            {/literal}
            {foreach $bunting_cart_products as $cartProduct}
            {literal}
            var bunting_price = {/literal}{$cartProduct.price_wt|escape:'htmlall':'UTF-8'}{literal};
            bunting_price = checkDecimal(bunting_price);

            $_Bunting.d.cp.push([
                {/literal}
                "{$cartProduct.id_product|escape:'htmlall':'UTF-8'}",
                parseInt(bunting_price).toFixed(2),
                "{$cartProduct.quantity|escape:'htmlall':'UTF-8'}"
                {literal}
            ]);
            {/literal}
            {/foreach}
            {literal}
            function checkDecimal(this_price) {
                this_price = this_price.toFixed(2).toString();
                var decimal_mark = this_price.charAt(this_price.length-3);
                if (decimal_mark == ',') {
                    this_price = this_price.replace(",", "*");
                    this_price = this_price.replace(".", "");
                    this_price = this_price.replace("*", ".");
                }
                return this_price;
            }
        </script>
    {/literal}
    {elseif $page['page_name'] == 'order'}
    {literal}
        <script type="text/javascript">
            if (typeof $_Bunting=="undefined") var $_Bunting={d:{}};
            $_Bunting.d.cp = new Array();

            var bunting_shipping = {/literal}{$cart->getTotalShippingCost()|escape:'htmlall':'UTF-8'}{literal};
            bunting_shipping = bunting_shipping.toFixed(2);
            $_Bunting.d.cdc = bunting_shipping.toString();

            {/literal}
            {foreach $cart->getProducts() as $cartProduct}
            {literal}
            var bunting_price = {/literal}{$cartProduct.price_wt|escape:'htmlall':'UTF-8'}{literal};
            bunting_price = checkDecimal(bunting_price);

            $_Bunting.d.cp.push([
                {/literal}
                "{$cartProduct.id_product|escape:'htmlall':'UTF-8'}",
                parseInt(bunting_price).toFixed(2),
                "{$cartProduct.quantity|escape:'htmlall':'UTF-8'}"
                {literal}
            ]);
            {/literal}
            {/foreach}
            {literal}
            function checkDecimal(this_price) {
                this_price = this_price.toFixed(2).toString();
                var decimal_mark = this_price.charAt(this_price.length-3);
                if (decimal_mark == ',') {
                    this_price = this_price.replace(",", "*");
                    this_price = this_price.replace(".", "");
                    this_price = this_price.replace("*", ".");
                }
                return this_price;
            }
        </script>
    {/literal}
    {elseif $page['page_name'] == 'order-confirmation' && $bunting_order}
    {literal}
        <script type="text/javascript">
            if (typeof $_Bunting=="undefined") var $_Bunting={d:{}};
            $_Bunting.vc = "";
            $_Bunting.d.uc = "{/literal}{$bunting.unique_code|escape:'htmlall':'UTF-8'}{literal}";
            $_Bunting.d.op = new Array();

            $_Bunting.d.uoc = "{/literal}{$bunting_order->id|escape:'htmlall':'UTF-8'}{literal}";

            var bunting_shipping = {/literal}{$bunting_order->total_shipping_tax_incl|escape:'htmlall':'UTF-8'}{literal};
            bunting_shipping = bunting_shipping.toFixed(2);

            $_Bunting.d.odc = bunting_shipping.toString();

            {/literal}{foreach $bunting_order->getProducts() as $orderProduct}{literal}
            var bunting_price = {/literal}{$orderProduct.unit_price_tax_incl|escape:'htmlall':'UTF-8'}{literal};
            bunting_price = checkDecimal(bunting_price);

            $_Bunting.d.op.push([
                {/literal}
                "{$orderProduct.product_id|escape:'htmlall':'UTF-8'}",
                parseInt(bunting_price).toFixed(2),
                "{$orderProduct.product_quantity|escape:'htmlall':'UTF-8'}"
                {literal}
            ]);
            {/literal}{/foreach}{literal}

            function checkDecimal(this_price) {
                this_price = this_price.toFixed(2).toString();
                var decimal_mark = this_price.charAt(this_price.length-3);
                if (decimal_mark == ',') {
                    this_price = this_price.replace(",", "*");
                    this_price = this_price.replace(".", "");
                    this_price = this_price.replace("*", ".");
                }
                return this_price;
            }
        </script>
    {/literal}
    {elseif $page['page_name'] == 'index'}
    {literal}
        <script type="text/javascript">if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; $_Bunting.d.hp = 'yes';</script>
    {/literal}
    {elseif $page['page_name'] == 'order'}
    {literal}
        <script type="text/javascript">if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; $_Bunting.d.co = 'yes';</script>
    {/literal}
    {elseif $page['page_name'] == 'category'}
    {literal}
        <script type="text/javascript">
            if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}};

            $_Bunting.d.c = "{/literal}{$bunting_category|escape:'htmlall':'UTF-8'}{literal}".replace(/&amp;/g, "&").replace(/&gt;/g, ">");
        </script>
    {/literal}
    {/if}

    {if $bunting_customer}
    {literal}
        <script type="text/javascript">
            if (typeof window.$_Bunting=='undefined') window.$_Bunting={d:{}};

            $_Bunting.d.uac = "{/literal}{$bunting_customer.id|escape:'htmlall':'UTF-8'}{literal}";
            $_Bunting.d.fn = "{/literal}{$bunting_customer.firstname|escape:'htmlall':'UTF-8'}{literal}";
            $_Bunting.d.sn = "{/literal}{$bunting_customer.lastname|escape:'htmlall':'UTF-8'}{literal}";
            $_Bunting.d.ea = "{/literal}{$bunting_customer.email|escape:'htmlall':'UTF-8'}{literal}";
            $_Bunting.d.g = "";
        </script>
    {/literal}
    {/if}

{literal}
    <script type="text/javascript">(function(){if(typeof window.$_Bunting=="undefined")window.$_Bunting={d:{}};$_Bunting.src=("https:"==document.location.protocol?"https://":"http://")+"{/literal}{$bunting.subdomain|escape:'htmlall':'UTF-8'}{literal}.bunting.com/call.js?wmID={/literal}{$bunting.website_monitor_id|escape:'htmlall':'UTF-8'}{literal}";$_Bunting.s=document.createElement("script");$_Bunting.s.type="text/javascript";$_Bunting.s.async=true;$_Bunting.s.defer=true;$_Bunting.s.charset="UTF-8";$_Bunting.s.src=$_Bunting.src;document.getElementsByTagName("head")[0].appendChild($_Bunting.s)})()</script>
{/literal}
{/if}