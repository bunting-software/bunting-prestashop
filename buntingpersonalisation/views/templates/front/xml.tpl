{*
* 2016 Bunting Software Ltd
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    Bunting Software Ltd <hello@bunti.ng>
* @copyright 2016 Bunting Software Ltd
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE feed SYSTEM "https://{$bunting_subdomain|escape:'htmlall':'UTF-8'}.bunting.com/feed-{$bunting_website_monitor_id|escape:'htmlall':'UTF-8'}.dtd">
<feed last_page="{$last_page|escape:'htmlall':'UTF-8'}">
    {foreach from=$products item=product}

    <product>
        <upc>{$product.id_product|escape:'htmlall':'UTF-8'}</upc>
        <ns>
            {foreach from=$product.names key=code item=name}
            <{$code|escape:'htmlall':'UTF-8'}><![CDATA[{$name|escape:'htmlall':'UTF-8'}]]></{$code|escape:'htmlall':'UTF-8'}>
            {/foreach}
        </ns>
        <ps>
            {foreach from=$product.prices key=code item=price}
            <{$code|escape:'htmlall':'UTF-8'}>{$price|escape:'htmlall':'UTF-8'}</{$code|escape:'htmlall':'UTF-8'}>
            {/foreach}
        </ps>
        <u><![CDATA[{$product.link|escape:'htmlall':'UTF-8'}]]></u>
        {if $product.image_link}<iu><![CDATA[{$product.image_link|escape:'htmlall':'UTF-8'}]]></iu>{/if}

        {if $product.category_name}<c><![CDATA[{$product.category_name}]]></c>{/if}

        {if $product.manufacturer_name}<b><![CDATA[{$product.manufacturer_name|escape:'htmlall':'UTF-8'}]]></b>{/if}

        {if $product.special_offer_savings}
            <oss>
            {foreach from=$product.special_offer_savings key=code item=saving}
                <{$code|escape:'htmlall':'UTF-8'}>{$saving|escape:'htmlall':'UTF-8'}</{$code|escape:'htmlall':'UTF-8'}>
            {/foreach}
            </oss>
        {/if}
        
        {if $product.special_offer_to}<oe>{$product.special_offer_to|escape:'htmlall':'UTF-8'}</oe>{/if}

        {if $product.out_of_stock == 2}<s>y</s>{else}<s>n</s>{/if}

        {if $product.meta_keywords}<kw><![CDATA[{$product.meta_keywords|escape:'htmlall':'UTF-8'}]]></kw>{/if}

        {if $product.ean13}<gtin><![CDATA[{$product.ean13|escape:'htmlall':'UTF-8'}]]></gtin>{/if}
    </product>
    {/foreach}

</feed>