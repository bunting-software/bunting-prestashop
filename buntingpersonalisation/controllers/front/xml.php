<?php
/**
 * 2016 Bunting Software Ltd
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Bunting Software Ltd <hello@bunti.ng>
 * @copyright 2016 Bunting Software Ltd
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class BuntingPersonalisationXmlModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        $this->display_header = false;
        $this->display_header_javascript = false;
        $this->display_footer = false;
        $this->content_only = true;
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();
        $feed_token = Tools::getValue('feed_token');

        if (Configuration::get('BUNTING_ACCOUNT_ID') && $feed_token === Configuration::get('BUNTING_FEED_TOKEN')) {
            $this->display_header = false;
            $this->display_header_javascript = false;
            $this->display_footer = false;
            $this->content_only = true;
            header('Content-Type: application/xml');
            try {
                $this->setTemplate('module:buntingpersonalisation/views/templates/front/xml.tpl');
            } catch (Exception $e) {
                echo $e->getMessage();
                $this->throwXmlError("500 Internal Server Error");
            }

            $this->context->smarty->assign($this->getFeedContent());
        } else {
            $this->throwXmlError("403 Forbidden");
        }
    }

    // This stops smarty outputting body/html tags by default
    public function getLayout()
    {
        return false;
    }

    /**
     * @param $error
     */
    private function throwXmlError($error) {
        header('HTTP/1.0 ' . $error);
        echo $error;
        die();
    }

    private function getFeedContent()
    {
        $feed_content = [];
        $feed_content['bunting_subdomain'] = Configuration::get('BUNTING_SUBDOMAIN');
        $feed_content['bunting_website_monitor_id'] = Configuration::get('BUNTING_WEBSITE_MONITOR_ID');
        $products_info = $this->getProducts();
        $feed_content['products'] = $products_info['products'];
        $feed_content['last_page'] = $products_info['last_page'];
        return $feed_content;
    }

    private function getProducts()
    {
        $productObj = new Product();

        if (Tools::getValue('size')) {
            $limit = (int) Tools::getValue('size');
            if ($limit <= 0) {
                $limit = 1;
            }
        } else {
            $limit = 200;
        }

        if (Tools::getValue('page')) {
            $page = (int) Tools::getValue('page');
            if ($page <= 0) {
                $page = 1;
            } else {
                $page = $page + 1;
            }
        } else {
            $page = 1;
        }

        $products = $productObj->getProducts(Configuration::get('PS_LANG_DEFAULT'), $page, $limit, 'date_add', 'ASC', false, true);

        $last_page = 'yes';

        if (count($products) == $limit) {
            $products = $productObj->getProducts(Configuration::get('PS_LANG_DEFAULT'), $page+1, $limit, 'date_add', 'ASC', false, true);

            if (count($products)) {
                $last_page = 'no';
            }
        }

        $categories = [];
        $protocol = Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://';
        $url_prefix = $protocol.Configuration::get('PS_SHOP_DOMAIN').'/index.php?controller=product&id_product=';

        $currencies = Currency::getCurrencies();
        $languages = Language::getLanguages();

        $processed_products = [];

        foreach ($products as $product) {
            $product['link'] = $url_prefix.$product['id_product'];
            $image = Image::getCover($product['id_product']);
            if (!$image) {
                continue;
            }
            $link = new Link;
            $image_url = $link->getImageLink(
                $product['link_rewrite'],
                $image['id_image'],
                ImageType::getFormatedName('home')
            );
            $product['image_link'] = $protocol.$image_url;

            $category_id = $product['id_category_default'];
            if (isset($categories[$category_id])) {
                $category_name = $categories[$category_id];
            } else {
                $categoryObj = new Category($category_id);
                $parents = $categoryObj->getParentsCategories();
                $this_cat_names = [];
                foreach($parents as $parent) {
                    $this_cat_names[] = $parent['name'];
                }
                $this_cat_names = array_reverse($this_cat_names);
                $categories[$category_id] = $category_name = implode('>',$this_cat_names);
            }

            $shop_id = Configuration::get('PS_SHOP_DEFAULT');
            $country_id = Configuration::get('PS_COUNTRY_DEFAULT');

            $product['names'] = [];
            foreach ($languages as $language) {
                $name = Product::getProductName($product['id_product'], null, $language['id_lang']);
                $product['names'][Tools::strtolower($language['iso_code'])] = $name;
            }
            $product['category_name'] = $category_name;
            $product['price'] = number_format(Product::getPriceStatic($product['id_product'], true, null, 2), 2);
            $default_currency_id = Configuration::get('PS_CURRENCY_DEFAULT');
            $specific_price = SpecificPrice::getSpecificPrice(
                $product['id_product'],
                $shop_id,
                $default_currency_id,
                $country_id,
                1,
                1,
                null,
                0,
                0,
                1
            );

            $product['prices'] = [];
            foreach ($currencies as $currency) {
                $currency_code = Tools::strtolower($currency['iso_code']);
                $product['prices'][$currency_code] = number_format($product['price'] * $currency['conversion_rate'], 2);
                if ($specific_price) {
                    if ($specific_price['reduction_type'] === "percentage") {
                        $priceBeforeDiscount = number_format($product['price'] / (1 - $specific_price['reduction']), 2);
                        $saving = number_format($priceBeforeDiscount * $specific_price['reduction'], 2);
                    } else if ($specific_price['reduction_type'] === "amount") {
                        $saving = number_format($specific_price['reduction'] * $currency['conversion_rate'], 2);
                    }

                    $product['special_offer_savings'][$currency_code] = $saving;

                    if (!isset($product['special_offer_to']) && isset($specific_price['to'])) {
                        $to = DateTime::createFromFormat('Y-m-d H:i:s', $specific_price['to']);

                        if ($to >= time()) {
                            $product['special_offer_to'] = $to->format('U');
                        }
                    }
                }
            }
            $processed_products[] = $product;
        }
        $products = $processed_products;
        return compact('products', 'last_page');
    }
}
